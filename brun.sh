#!/bin/bash

if [[ $1 == "ast" ]]; then
  echo "generating AST"
  kotlinc -script generateAst.kts `pwd`/src/main/kotlin/com/gadomski/klox/ast
elif [[ $1 == "all" ]]; then
  echo "generating AST"
  kotlinc -script generateAst.kts `pwd`/src/main/kotlin/com/gadomski/klox/ast
  echo "building klox"
  ./gradlew clean build
  ./run.sh
else 
  echo "building klox"
  ./gradlew clean build
  ./run.sh $@
fi
