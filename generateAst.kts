import java.io.PrintWriter
import java.util.*

fun defineAst(path: String, classToTypes: Map<String, Map<String, List<String>>>) {

    classToTypes.entries.forEach { (baseClass, types) ->
        val pathname = "$path/$baseClass.kt"
        val file = java.io.File(pathname)
        if (!file.exists()) file.createNewFile()

        val writer = PrintWriter(pathname, "UTF-8")
        writer.println("package com.gadomski.klox.ast\n");
        writer.println("import com.gadomski.klox.*\n");

        writer.println("abstract class $baseClass {")
        writer.println("    abstract fun <R> accept(visitor: Visitor<R>): R\n")
        defineVisitor(writer, types.keys, baseClass.toLowerCase())

        types.entries
                .forEach { (className, fields) -> defineClass(writer, className, baseClass, fields) }

        writer.println("}\n")
        writer.close();
    }

}

fun defineClass(writer: PrintWriter, className: String, baseClass: String, fields: List<String>) {
    writer.print("    class $className(")
    fields.forEachIndexed { index, it ->
        val split = it.split(" ".toRegex())
        val type = split[0]
        val name = split[1]
        writer.print("val $name: $type")
        if (index == fields.size - 1)
            writer.print(") : $baseClass() {\n")
        else
            writer.print(",")
    }
    writer.println("        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)")
    writer.println("    }\n")
}

fun defineVisitor(writer: PrintWriter, types: Set<String>, variableName: String) {
    writer.println("    interface Visitor<R> {");
    types.forEach {
        writer.println("        fun visit($variableName: $it): R")
    }
    writer.println("    }\n");
}

val exprTypes = mapOf(
        "Assign" to listOf("Token name", "Expr value"),
        "Binary" to listOf("Expr left", "Token operator", "Expr right"),
        "Logical" to listOf("Expr left", "Token operator", "Expr right"),
        "Grouping" to listOf("Expr expression"),
        "Literal" to listOf("Any? value"),
        "Unary" to listOf("Token operator", "Expr right"),
        "Variable" to listOf("Token name")
)


val stmtTypes = mapOf(
        "Block" to listOf("List<Stmt> statements"),
        "Expression" to listOf("Expr expression"),
        "If" to listOf("Expr condition", "Stmt thenBranch", "Stmt? elseBranch"),
        "Print" to listOf("Expr expression"),
        "Var" to listOf("Token name", "Expr? initializer"),
        "While" to listOf("Expr condition", "Stmt body")
)

if (args.size != 1) {
    System.err.println("Usage: generate_ast <output directory>");
    System.exit(1);
}

defineAst(args[0],
        mapOf("Expr" to exprTypes,
                "Stmt" to stmtTypes))
