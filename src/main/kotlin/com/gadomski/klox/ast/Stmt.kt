package com.gadomski.klox.ast

import com.gadomski.klox.*

abstract class Stmt {
    abstract fun <R> accept(visitor: Visitor<R>): R

    interface Visitor<R> {
        fun visit(stmt: Block): R
        fun visit(stmt: Expression): R
        fun visit(stmt: If): R
        fun visit(stmt: Print): R
        fun visit(stmt: Var): R
        fun visit(stmt: While): R
    }

    class Block(val statements: List<Stmt>) : Stmt() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class Expression(val expression: Expr) : Stmt() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class If(val condition: Expr,val thenBranch: Stmt,val elseBranch: Stmt?) : Stmt() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class Print(val expression: Expr) : Stmt() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class Var(val name: Token,val initializer: Expr?) : Stmt() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class While(val condition: Expr,val body: Stmt) : Stmt() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

}

