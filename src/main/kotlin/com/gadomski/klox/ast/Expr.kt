package com.gadomski.klox.ast

import com.gadomski.klox.*

abstract class Expr {
    abstract fun <R> accept(visitor: Visitor<R>): R

    interface Visitor<R> {
        fun visit(expr: Assign): R
        fun visit(expr: Binary): R
        fun visit(expr: Logical): R
        fun visit(expr: Grouping): R
        fun visit(expr: Literal): R
        fun visit(expr: Unary): R
        fun visit(expr: Variable): R
    }

    class Assign(val name: Token,val value: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class Binary(val left: Expr,val operator: Token,val right: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class Logical(val left: Expr,val operator: Token,val right: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class Grouping(val expression: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class Literal(val value: Any?) : Expr() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class Unary(val operator: Token,val right: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

    class Variable(val name: Token) : Expr() {
        override fun <R> accept(visitor: Visitor<R>) = visitor.visit(this)
    }

}

