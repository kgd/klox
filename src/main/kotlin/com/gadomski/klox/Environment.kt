package com.gadomski.klox

import java.util.*

class Environment(private val values: MutableMap<String, Any?> = HashMap(), val enclosing: Environment? = null) {

    fun define(name: String, value: Any?) {
        values[name] = value;
    }

    fun get(name: Token): Any? {
        return values.getOrElse(name.lexeme, {
            if (enclosing != null) return enclosing.get(name)
            throw RuntimeError(name, "Undefined variable")
        })
    }

    fun assign(name: Token, value: Any?) {
        if (values[name.lexeme] == null) {
            if (enclosing != null)
                return enclosing.assign(name, value)
            else
                throw RuntimeError(name, "Undefined variable ${name.lexeme}.")
        }
        values[name.lexeme] = value
    }
}