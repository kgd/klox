package com.gadomski.klox

import com.gadomski.klox.TokenType.*
import java.util.*

internal class Scanner(private val source: String, private val errorManager: ErrorManager) {
    private val tokens = ArrayList<Token>()
    private var start = 0
    private var current = 0
    private var line = 1

    fun scanTokens(): List<Token> {
        while (!isAtEnd()) {
            this.start = current
            scanToken()
        }
        tokens.add(Token(EOF, "", line))
        return tokens
    }

    private fun isAtEnd(): Boolean {
        return current >= source.length
    }

    private fun scanToken() {
        val c = advance()
        when (c) {
            '(' -> addToken(LEFT_PAREN)
            ')' -> addToken(RIGHT_PAREN)
            '{' -> addToken(LEFT_BRACE)
            '}' -> addToken(RIGHT_BRACE)
            ',' -> addToken(COMMA)
            '.' -> addToken(DOT)
            '-' -> addToken(MINUS)
            '+' -> addToken(PLUS)
            ';' -> addToken(SEMICOLON)
            '*' -> addToken(STAR)
            '!' -> addToken(if (match('=')) BANG_EQUAL else BANG)
            '=' -> addToken(if (match('=')) EQUAL_EQUAL else EQUAL)
            '<' -> addToken(if (match('=')) LESS_EQUAL else LESS)
            '>' -> addToken(if (match('=')) GREATER_EQUAL else GREATER)
            '/' -> if (match('/')) skipComment() else addToken(SLASH)
            '"' -> addLiteral()
            '\n' -> line++
            ' ',
            '\r',
            '\t' -> {}
            else ->
                when {
                    isDigit(c) -> addNumber()
                    isAlpha(c) -> addIdentifier()
                    else -> errorManager.error(line, "Unexpected character")
                }
        }
    }

    private fun addToken(type: TokenType, literal: Any = NoLiteral) {
        val text = source.substring(start, current)
        tokens.add(Token(type, text, line, literal))
    }

    private fun addLiteral() {
        while (peek() != '"' && !isAtEnd()) {
            if (peek() == '\n') line++
            advance()
        }
        // Unterminated string.
        if (isAtEnd()) {
            errorManager.error(line, "Unterminated string.")
            return
        }
        // The closing ".
        advance()
        // Trim the surrounding quotes.
        val value = source.substring(start + 1, current - 1)
        addToken(STRING, value)
    }

    private fun addNumber() {
        while (isDigit(peek())) advance();
        // Look for a fractional part.
        if (peek() == '.' && isDigit(peekNext())) {
            // Consume the "."
            advance();
            while (isDigit(peek())) advance();
        }
        addToken(NUMBER, source.substring(start, current).toDouble())
    }

    private fun addIdentifier() {
        while (isAlphaNumeric(peek())) advance()
        val text = source.substring(start, current)
        addToken(Keywords.get(text))
    }

    private fun skipComment() {
        while (peek() != '\n' && !isAtEnd()) advance()
    }

    private fun advance(): Char {
        return source[current++]
    }

    private fun match(expected: Char): Boolean {
        if (isAtEnd()) return false
        if (source[current] != expected) return false
        current++
        return true
    }

    private fun peek(): Char {
        return if (isAtEnd()) '\u0000' else source[current]
    }

    private fun peekNext(): Char {
        return if (current + 1 >= source.length) '\u0000' else source[current + 1]
    }

    private fun isDigit(c: Char): Boolean {
        return c in '0'..'9'
    }

    private fun isAlpha(c: Char): Boolean {
        return c in 'a'..'z' ||
                c in 'A'..'Z' ||
                c == '_'
    }

    private fun isAlphaNumeric(c: Char): Boolean {
        return isAlpha(c) || isDigit(c)
    }
}