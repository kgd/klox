/*
package com.gadomski.klox

import com.gadomski.klox.ast.*

internal class AstPrinter : Expr.Visitor<String> {
    fun print(expr: Expr): String {
        return expr.accept(this)
    }

    override fun visit(expr: Expr.Variable): String {
        return expr.name.lexeme
    }

    override fun visit(expr: Expr.Binary): String {
        return parenthesize(expr.operator.lexeme, expr.left, expr.right)
    }

    override fun visit(expr: Expr.Grouping): String {
        return parenthesize("group", expr.expression)
    }

    override fun visit(expr: Expr.Literal): String {
        return if (expr.value == null) "nil" else expr.value.toString()
    }

    override fun visit(expr: Expr.Unary): String {
        return parenthesize(expr.operator.lexeme, expr.right)
    }

    private fun parenthesize(name: String, vararg expressions: Expr): String {
        val builder = StringBuilder()
        builder.append("(").append(name)
        for (expr in expressions) {
            builder.append(" ")
            builder.append(expr.accept(this))
        }
        builder.append(")")
        return builder.toString()
    }
}*/
