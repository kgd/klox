package com.gadomski.klox

class ErrorManager(private var hadError: Boolean = false, private var hadRuntimeError: Boolean = false  ) {

    fun hadError() = this.hadError
    fun hadRuntimeError() = this.hadRuntimeError

    fun clear() {
        this.hadError = false
    }

    fun error(line: Int, message: String) {
        report(line, "", message)
    }

    fun error(token: Token, message: String) {
        if (token.type === TokenType.EOF) {
            report(token.line, " at end", message)
        } else {
            report(token.line, " at '" + token.lexeme + "'", message)
        }
    }

    private fun report(line: Int, where: String, message: String) {
        this.hadError = true
        System.err.println("[line $line] Error$where: $message")
    }

    fun runtimeError(error: RuntimeError) {
        System.err.println("${error.message}\n[line ${error.token.line}]")
        hadRuntimeError = true
    }
}