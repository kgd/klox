package com.gadomski.klox

import java.io.BufferedReader
import java.io.InputStreamReader
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths

internal class KLox(private val errorManager: ErrorManager = ErrorManager()) {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            if (args.size > 1) {
                println("Usage: klox [script]")
                return
            }
            val kLox = KLox()
            if (args.size == 1) {
                kLox.runFile(args.get(0))
            } else {
                kLox.runPrompt()
            }
        }
    }

    private val interpreter: Interpreter = Interpreter(errorManager)

    private fun runFile(path: String) {
        val bytes = Files.readAllBytes(Paths.get(path))
        run(String(bytes, Charset.defaultCharset()))
        if (errorManager.hadError()) System.exit(65);
        if (errorManager.hadRuntimeError()) System.exit(70);
    }

    private fun runPrompt() {
        val input = InputStreamReader(System.`in`)
        val reader = BufferedReader(input)
        while (true) {
            print("> ")
            run(reader.readLine())
            errorManager.clear()
        }
    }

    private fun run(source: String) {
        val scanner = Scanner(source, errorManager)
        val parser = Parser(scanner.scanTokens(), errorManager)
        val statements = parser.parse()
        // Stop if there was a syntax error.
        if (errorManager.hadError()) return
        interpreter.interpret(statements)
    }

}