package com.gadomski.klox

import com.gadomski.klox.TokenType.*
import com.gadomski.klox.ast.Expr
import com.gadomski.klox.ast.Stmt

class Interpreter(private val errorManager: ErrorManager) : Expr.Visitor<Any?>, Stmt.Visitor<Unit> {
    private var environment: Environment = Environment()

    fun interpret(stmts: List<Stmt>) {
        try {
            stmts.forEach { execute(it) }
        } catch (error: RuntimeError) {
            errorManager.runtimeError(error)
        }
    }


    private fun evaluate(expr: Expr): Any? {
        return expr.accept(this)
    }

    private fun execute(stmt: Stmt) {
        stmt.accept(this)
    }

    override fun visit(stmt: Stmt.While) {
        while (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.body)
        }
    }

    override fun visit(expr: Expr.Logical): Any? {
        val left = evaluate(expr.left)
        if (expr.operator.type == OR) {
            if (isTruthy(left)) return left
        } else {
            if (!isTruthy(left)) return left
        }
        return isTruthy(evaluate(expr.right))
    }

    override fun visit(stmt: Stmt.Block) {
        executeBlock(stmt.statements, Environment(enclosing = this.environment))
    }

    private fun executeBlock(statements: List<Stmt>, environment: Environment) {
        val previous = this.environment
        try {
            this.environment = environment
            for (statement in statements) {
                execute(statement)
            }
        } finally {
            this.environment = previous
        }
    }

    override fun visit(stmt: Stmt.If) {
        if (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.thenBranch)
        } else if (stmt.elseBranch != null) {
            execute(stmt.elseBranch)
        }
    }

    override fun visit(expr: Expr.Assign): Any? {
        val value = evaluate(expr.value)
        environment.assign(expr.name, value)
        return value
    }

    override fun visit(expr: Expr.Variable): Any? {
        return environment.get(expr.name)
    }

    override fun visit(stmt: Stmt.Var): Unit {
        var value: Any? = null
        if (stmt.initializer != null) {
            value = evaluate(stmt.initializer)
        }
        environment.define(stmt.name.lexeme, value)
    }

    override fun visit(stmt: Stmt.Expression): Unit {
        evaluate(stmt.expression)
    }

    override fun visit(stmt: Stmt.Print): Unit {
        println(stringify(evaluate(stmt.expression)))
    }

    override fun visit(expr: Expr.Binary): Any? {
        val left = evaluate(expr.left)
        val right = evaluate(expr.right)
        validateBinary(expr.operator, left, right)
        return when (expr.operator.type) {
            PLUS -> if (left is Double && right is Double) left + right else if (left is String && right is String) left + right else null
            MINUS -> left as Double - right as Double
            SLASH -> left as Double / right as Double
            STAR -> left as Double * right as Double
            GREATER -> left as Double > right as Double
            GREATER_EQUAL -> left as Double >= right as Double
            LESS -> (left as Double) < right as Double
            LESS_EQUAL -> left as Double <= right as Double
            BANG_EQUAL -> !isEqual(left, right)
            EQUAL_EQUAL -> isEqual(left, right)
            else -> null //unreachable
        }
    }

    private fun validateBinary(token: Token, left: Any?, right: Any?) {
        when (token.type) {
            PLUS -> if ((left is Double && right is Double) || (left is String && right is String)) return else throw RuntimeError(token, "Both operands must be strings or numbers")
            MINUS,
            STAR,
            GREATER,
            GREATER_EQUAL,
            LESS,
            LESS_EQUAL -> areNumbers(token, left, right)
            SLASH -> {
                areNumbers(token, left, right)
                if (right == 0.toDouble()) throw RuntimeError(token, "Cannot divide by 0")
            }
            else -> return
        }

    }

    private fun areNumbers(token: Token, left: Any?, right: Any?) {
        if (!(left is Double && right is Double)) throw RuntimeError(token, "Operands must be numbers")
    }

    override fun visit(expr: Expr.Grouping): Any? {
        return evaluate(expr.expression)
    }

    override fun visit(expr: Expr.Literal): Any? {
        return expr.value
    }

    override fun visit(expr: Expr.Unary): Any? {
        val value = evaluate(expr.right)
        return when (expr.operator.type) {
            MINUS -> value as Double * -1
            BANG -> !isTruthy(value)
            else -> null //unreachable code
        }
    }

    private fun isTruthy(value: Any?): Boolean {
        if (value == null) return false
        if (value is Boolean) return value
        return true
    }

    private fun isEqual(a: Any?, b: Any?): Boolean {
        // nil is only equal to nil.
        if (a == null && b == null) return true
        return if (a == null) false else a == b
    }

    private fun stringify(value: Any?): String {
        if (value == null) return "nil"
        if (value is Double) {
            val text = value.toString()
            if (text.endsWith(".0")) return text.substring(0, text.length - 2)
            return text
        }
        return value.toString()
    }
}

class RuntimeError(val token: Token, message: String) : RuntimeException(message)
