package com.gadomski.klox

class Token(val type: TokenType, val lexeme: String, val line: Int, val literal: Any = NoLiteral) {
    override fun toString(): String {
        return type.toString() + " " + lexeme + " " + literal
    }
}

object NoLiteral {
    override fun toString(): String {
        return ""
    }
}